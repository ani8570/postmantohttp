from util import *
import re

class Converter:
    def to_http(self):
        print('to_http')

    def write_http(self):
        print('write_http')


class JsonConverter(Converter):
    def write_http(self, f, json_dict):
        if json_dict['request']['method'] == 'VIEW':
            return
        f.write('\n')
        f.write('### ' + json_dict['name'] + '\n')
        url = (json_dict['request']['url']['protocol']
               + '://{{url}}/'
               + '/'.join(json_dict['request']['url']['path'])
               )
        f.write(json_dict['request']['method'] + ' ' + url + '\n')
        for header in json_dict['request']['header']:
            f.write(header['key'] + ': ' + header['value'] + '\n')
        f.write('\n')
        try:
            s = json_dict['request']['body']['raw'].replace('\r\n', '\n')
            f.write(s)
        except:
            pass

    def to_http(self, folder_path, file_path):
        json_map = read_json(file_path)
        create_folder(os.path.abspath(os.path.join(folder_path, json_map['info']['name'])))
        file_counter = 1
        for item_jsons in json_map['item']:
            name = re.sub(r"[:\\/?\*\"<>]", "", item_jsons['name'])
            f = open(
                os.path.abspath(
                    os.path.join(folder_path, json_map['info']['name'], format(file_counter, '03') + ". " + name)) + '.http',
                'w',
                encoding='utf-8')
            file_counter += 1
            if 'item' in item_jsons:
                for item_json in item_jsons['item']:
                    self.write_http(f, item_json)
            else:
                self.write_http(f, item_jsons)
            f.close()


class RobotConverter(Converter):
    def write_http(self, f, json_dict):
        if json_dict['request']['method'] == 'VIEW':
            return
        f.write('\n')
        f.write('### ' + json_dict['name'] + '\n')
        url = (json_dict['request']['url']['protocol']
               + '://{{url}}/'
               + '/'.join(json_dict['request']['url']['path'])
               )
        f.write(json_dict['request']['method'] + ' ' + url + '\n')
        for header in json_dict['request']['header']:
            f.write(header['key'] + ': ' + header['value'] + '\n')
        f.write('\n')
        try:
            s = json_dict['request']['body']['raw'].replace('\r\n', '\n')
            f.write(s)
        except:
            pass

    def to_http(self, folder_path, file_path):
        json_map = read_json(file_path)
        create_folder(os.path.abspath(os.path.join(folder_path, json_map['info']['name'])))
        file_counter = 1
        for item_jsons in json_map['item']:
            name = re.sub(r"[:\\/?\*\"<>]", "", item_jsons['name'])
            f = open(
                os.path.abspath(
                    os.path.join(folder_path, json_map['info']['name'], format(file_counter, '03') + ". " + name)) + '.http',
                'w',
                encoding='utf-8')
            file_counter += 1
            if 'item' in item_jsons:
                for item_json in item_jsons['item']:
                    self.write_http(f, item_json)
            else:
                self.write_http(f, item_jsons)
            f.close()