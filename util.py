import json, os

def read_json(file: str):
    with open(file, 'r', encoding='utf-8') as f:
        json_data = json.load(f)

    return json_data


def create_folder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print('Error: Creating directory. ' + directory)
