# This is a sample Python script.
import os
import sys

from converter import *

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    if len(sys.argv) < 2:
        folder_path = os.path.abspath(os.path.join(os.getcwd(), 'sample'))
    else:
        folder_path = sys.argv[0]
    file_list = os.listdir(folder_path)

    for file in file_list:
        file_path = os.path.abspath(os.path.join(folder_path, file))
        converter = Converter()
        if file.endswith('.json'):
            converter = JsonConverter()
            converter.to_http(folder_path, file_path)
        # elif file.endswith('.robot'):
        #     f = open(file_path, 'r')
        #     for i in f.readlines():
        #         line = i.split("    ")
        #
